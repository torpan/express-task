fetch('/data')
	.then(function (response) {
		return response.json()
	})
	.then (function (data) {
		appendData(data)
	})
	.catch (function (err) {
		console.log('error: '+ err)
	})

//Function to create elements under my "myData"-tag. Goes through my json and for each element it will
//create a new element. The if makes sure that only two will be beside eachother, then it will create a ner row.
//There is also another for-loop in order to go through the reviews and create new elements with them. 

function appendData(data) {
	console.log('entered function appendData')
	let mainContainer = document.getElementById("myData")
	for (let i = 0; i < data.restaurant.length; i++) {
		console.log('entered for-loop')
				
		
		if (i % 2 == 0)
		{
			let row = document.createElement("div")
			row.id = "row" + i
			row.className = "row"
			console.log(i)
			let item = document.createElement("div")
			item.className = 'item'
			let restaurantName = document.createElement("p")
			let restaurantLoc = document.createElement("p")
			let restaurantDesc = document.createElement("p")
			let restaurantImg = document.createElement("img")
			let restaurantRate = document.createElement("p")

			restaurantName.innerHTML = 'Name: ' + data.restaurant[i].name
			item.appendChild(restaurantName)
			restaurantLoc.innerHTML = 'Location: ' + data.restaurant[i].location
			item.appendChild(restaurantLoc)
			restaurantDesc.innerHTML = 'Description: ' + data.restaurant[i].description
			item.appendChild(restaurantDesc)
			restaurantImg.src = data.restaurant[i].image
			item.appendChild(restaurantImg)
			restaurantRate.innerHTML = 'Average rating: ' + data.restaurant[i].rating + ' stars'
			item.appendChild(restaurantRate)

			for(let j = 0; j < data.restaurant[i].reviews.length; j++) {
				console.log('entered second for')
				let restaurantRev = document.createElement("p")
				restaurantRev.innerHTML = 'Name: ' + data.restaurant[i].reviews[j].name + ' Comment: ' + data.restaurant[i].reviews[j].comments
				item.appendChild(restaurantRev)
			}
			row.appendChild(item)
			mainContainer.appendChild(row)
		}
		else {
			let row = document.getElementById("row" + (i - 1))
			row.className = "row"
			let item = document.createElement("div")
			item.className = 'item'
			let restaurantName = document.createElement("p")
			let restaurantLoc = document.createElement("p")
			let restaurantDesc = document.createElement("p")
			let restaurantImg = document.createElement("img")
			let restaurantRate = document.createElement("p")

			restaurantName.innerHTML = 'Name: ' + data.restaurant[i].name
			item.appendChild(restaurantName)
			restaurantLoc.innerHTML = 'Location: ' + data.restaurant[i].location
			item.appendChild(restaurantLoc)
			restaurantDesc.innerHTML = 'Description: ' + data.restaurant[i].description
			item.appendChild(restaurantDesc)
			restaurantImg.src = data.restaurant[i].image
			item.appendChild(restaurantImg)
			restaurantRate.innerHTML = 'Average rating: ' + data.restaurant[i].rating + ' stars'
			item.appendChild(restaurantRate)

			for(let j = 0; j < data.restaurant[i].reviews.length; j++) {
				console.log('entered second for')
				let restaurantRev = document.createElement("p")
				restaurantRev.innerHTML = 'Name: ' + data.restaurant[i].reviews[j].name + ' Comment: ' + data.restaurant[i].reviews[j].comments
				item.appendChild(restaurantRev)
			}
			row.appendChild(item)
			mainContainer.appendChild(row)
		}		
	}
	console.log('left the foorloop')
}


