const path = require('path')
const restaurantJSON = require( path.join(__dirname, 'public', 'data.json'))
const express = require('express')
const app = express()
const { PORT = 8080 } = process.env

//Allowing the express server to share static content
app.use(express.static( path.join(__dirname, 'public', 'static') ))
//Adding JSON Middleware 
app.use(express.json())
//Adding ability to parse nested form data
app.use(express.urlencoded({ extended: true }))


//Default route for my website
app.get('/', (req, res) => {
	console.log('Entered the indexpage')
	return res.sendFile( path.join(__dirname, 'public', 'index.html'))
})

//Data page Route for my website, to display data as JSON. Reads a file and parses it. 
app.get('/data', (req, res) => {
	console.log('Entered the datapage')
	return res.status(200).json(restaurantJSON)
	})

//Restaurants page Route for my website, to display data in html
app.get('/restaurants', (req, res) => {
	console.log('Entered the fancy restaurants')
	return res.sendFile( path.join(__dirname, 'public', 'restaurants.html'))
})

app.listen(PORT, () => console.log('Server started on port 8080...'))